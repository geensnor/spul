# Geensnor over spullen
De [Wirecutter](https://thewirecutter.com/) van Geensnor. Wij zorgen dat jij wél het goede spul koopt! Weet je ook nog goed of slecht spul? Doe mee! [https://bitbucket.org/geensnor/spul](https://bitbucket.org/geensnor/spul)


## Electronica
Wat | Naam | URL | Goed?
--- | --- | --- |---
Bluetooth speaker | Dali Katch | [dali-speakers.com](https://www.dali-speakers.com/nl/loudspeakers/active/dali-katch/)|Ja, super relaxed
Bluetooth headset|Jabra Move|[jabra.nl](https://www.jabra.nl/music/jabra-move-wireless)|Ja, aardig. Verbinding is niet super stabiel. Wel degelijk ding.
Mediaspeler|Minix NEO U1|[mimixwebshop.nl](https://www.minixwebshop.nl/minix-neo-u1.html)|Nee, echt heel slecht. Nooit kopen. Ik heb er trouwens nog een liggen. Kopen?
E-Reader|Kobo Aura|[kobobooks.com](https://gl.kobobooks.com/products/kobo-aura)|Ja, prima. Koppelen met bol.com account. Zelf epub's erop zetten en artikelen lezen met [Pocket](https://getpocket.com)
Draagbaar lampje|Waka Waka|[waka-waka.com](https://shop.waka-waka.com/product/waka-waka-light/)|Ja. Handig opladen met het zonnepaneeltje en dan gaat hij heel lang mee. Vooral de standaard werkt erg handig. Bedrijf was wel bijna falliet onlangs.
Oordopjes|KZ ATE|[dx.com](https://www.dx.com/p/kz-ate-universal-3-5mm-plug-in-ear-earphone-translucent-black-428748)|Ja. Super goedkoop maar klinken goed en vallen niet uit je oren. Geen bluetooth dus let op met iPhone etc.
Horloge|Garmin Vivomove HR| [garmin.com](https://buy.garmin.com/nl-NL/NL/p/583562)|Ja, lange batterij duur, normale wijzerplaat, en handige "smart dingen" en best stevig.

## Fiets
Racefiets of gewone fiets.

Wat | Naam | URL | Goed?
--- | --- | --- |---
Smeermiddel ketting|Squirt Lube Ketting Wax|[squirt-lube.be](http://www.squirt-lube.be/)|Ik ben nog niet overtuigd. Fiets kraakt meer en sneller dan eerst.
Fiets computer|Garmin Edge 520|[garmin.com](https://buy.garmin.com/nl-NL/NL/p/166370)|Ja prima.
Bidon|Cube Bidon. Waarschijnlijk allemaal|[cube.eu](https://www.cube.eu/nl/equipment/accessories/bottles/product/cube-bottle-075l-icon-red/)|Nee. Doordat het mondstuk te klein is valt er niet fatsoendelijk uit te drinken.
Bidon|Tacx Shiva|[tacx.com](https://tacx.com/nl/product/shiva/)|Ja, kei goedkoop en prima.
Verlichting|VIOO CLIP 500|[declaton.nl](https://www.decathlon.nl/p/set-led-fietsverlichting-vioo-clip-500-voor-en-achterlicht-zwart-usb/_/R-p-301360?mc=8501200&c=ZWART)|Ja. Handige clipjes erbij. En opladen met USB zodat je ze wat langer meegaan.

## Bier
Naam | URL | Goed?
 --- | --- |---
Saison de Dottignies|[deranke.be](http://www.deranke.be/nl/bier/saison-de-dottignies)|Ja, erg lekker
Kompaan Bloedbroeder|[kompaanbier.nl](http://kompaanbier.nl/beer)|Nee, beetje te heftige stout. Doordat er meer dan 9% alcohol inzit is het ook niet echt een lekkere doordrinker.
Grolsch Weizen-IPA|[Grolsch.nl](https://www.grolsch.nl/bieren/grolsch-weizen-ipa.html)|Nee. Niet erg lekker. Vond het al een wonderlijke combinatie en ik heb het idee dat het qua smaak overal een beetje lafjes tussenin hangt.

## Software
Wat | Platform | Naam | URL | Goed?
--- | --- | --- | --- | ---
Markdown Editor | macOS | MacDown | [macdown.uranusjr.com](https://macdown.uranusjr.com/) | Ja, prima


## Keuken
Wat | Naam | URL | Goed?
---|---|---|---
Tostie-ijzer|Proline contactgrill MGP12|[bol.com](https://www.bol.com/nl/p/proline-contactgrill-mgp12/9200000083500206/)|Nee. Tenzij je graag een uur op je tostie wacht. Wel lekker groot maar het schiet gewoon voor geen meter op.